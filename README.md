# Log in
path: "/v1/signin"  
method: POST  
header: -  
parameters: [parameters](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/sign_in.json)  
response: token

# Log in With Facebook
path: "/v1/signinwithfacebook"  
method: POST  
header: -  
parameters: [parameters](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/sign_in_with_token.json)  
response: token

# Log in With Google
path: "/v1/signinwithgoogle"  
method: POST  
header: -  
parameters: [parameters](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/sign_in_with_token.json)  
response: token

# Register
path: "/v1/signup"  
method: POST  
header: -  
parameters: [parameters](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/sign_up.json)  
response: token and token_verified(use in conpanies type only)

#Verify Email
path: "/v1/comfirm_email"  
method: POST  
header: -  
parameters: [parameters](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/comfirm_email.json)  
response: message

# Change Password
path: "/v1/change_password"  
method: POST  
header: token  
parameters: [parameters](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/change_password.json)  
response: message


# Get user profile(Me)
path: "/v1/profile"  
method: GET  
header: token  
parameters: -  
response: [response](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/profile.json)  

# Get user profile(Other)
path: "/v1/profile/:userid"  
method: GET  
header: token  
parameters: -  
response: [response](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/profile.json)  


# Edit normal profile
path: "/v1/profile"  
method: POST  
header: token  
parameters: [parameters](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/edit_profile.json)  
response: [response](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/profile.json) 

# Edit about
path: "/v1/profile"  
method: POST  
header: token  
parameters: [parameters](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/edit_about.json)  
response: [response](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/profile.json) 

# Edit contact
path: "/v1/profile"  
method: POST  
header: token  
parameters: [parameters](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/edit_contact.json)  
response: [response](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/profile.json) 

# Edit social
path: "/v1/profile"  
method: POST  
header: token  
parameters: [parameters](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/edit_social.json)  
response: [response](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/profile.json) 

# Edit address
path: "/v1/profile"  
method: POST  
header: token  
parameters: [parameters](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/edit_social.json)  
response: [response](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/profile.json)  

# Upload image profile
path: "/v1/profile"  
method: POST  
header: token  
parameters: profile_pic (File)  
response: [response](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/profile.json)  

# Set default template
path: "/v1/template/default"  
method: POST  
header: token  
parameters: template_id  
response: [response](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/profile.json) 
# Get all social in app
path: "/v1/socials"  
method: GET  
header: -  
parameters: -  
response: [response](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/app_social.json)  

# Get connection list
path: "/v1/connection"  
method: GET  
header: token  
parameters: -  
response: [response](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/connection.json)  

# Add connection
path: "/v1/connection"  
method: POST  
header: token  
parameters: user_id  
response: [response](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/connection.json)  

# Disconnect
path: "/v1/disconnect"  
method: POST  
header: token  
parameters: user_id  
response: [response](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/connection.json)  


#Create staff
path: "/v1/staff"  
method: POST  
header: token  
parameters: [parameters](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/request_staff.json)  
response: [response](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/staff.json)  

#Edit profile staff
path: "/v1/staff/:id"  
method: PUT  
header: token  
parameters: [parameters](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/staff.json)  
response: [response](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/staff.json)  



# Get all staff in company
path: "/v1/staffs"  
method: GET  
header: token  
parameters: fullname (optional),department (optional) 
response: [response](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/staffs.json)  

# Get one staff by id in company
path: "/v1/staff/:id"  
method: GET  
header: token  
parameters: -  
response: [response](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/staff.json)  

# Create Notification
path: "/v1/notification"  
method: POST  
header: token  
parameters: [parameters](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/notification_request.json)  
response: [response](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/notification_response.json)  

# Get All Notication of user
path: "/v1/notifications"  
method: GET  
header: token  
parameters: -  
response: [response](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/notification_response.json)  

# Get Only one Notication of user for mark read this notification
path: "/v1/notification/:id"  
method: GET  
header: token  
parameters: -  
response: [response](https://bitbucket.org/PattanapongJW/qrontact-api-document/src/master/notification_response.json)  